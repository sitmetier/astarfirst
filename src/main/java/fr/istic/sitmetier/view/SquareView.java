package fr.istic.sitmetier.view;

import javafx.scene.shape.Rectangle;

/**
 * Created by erwann on 11/03/15.
 */
public class SquareView extends Rectangle {

    private int x;
    private int y;

    public SquareView (int x, int y, int size){
        super(size,size);
        this.x = x;
        this.y = y;
    }

    public int getSquareX() {
        return x;
    }

    public int getSquareY() {
        return y;
    }
}
