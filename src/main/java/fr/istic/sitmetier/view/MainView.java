package fr.istic.sitmetier.view;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

/**
 * Created by erwann on 16/03/15.
 */
public class MainView extends Application {

    public static void main(String[] args){
        launch(args);
    }

    /**
     * The main entry point for all JavaFX applications.
     * The start method is called after the init method has returned,
     * and after the system is ready for the application to begin running.
     * <p/>
     * <p>
     * NOTE: This method is called on the JavaFX Application Thread.
     * </p>
     *
     * @param primaryStage the primary stage for this application, onto which
     *                     the application scene can be set. The primary stage will be embedded in
     *                     the browser if the application was launched as an applet.
     *                     Applications may create other stages, if needed, but they will not be
     *                     primary stages and will not be embedded in the browser.
     */
    @Override
    public void start(Stage primaryStage) throws Exception {

        final MatrixView matrixView = new MatrixView(100,50);

        final BorderPane mainPane = new BorderPane();
        mainPane.setCenter(matrixView.getDisplay());
        final FlowPane bottom = new FlowPane(Orientation.HORIZONTAL);
        Button drawPath = new Button("Draw Path");
        Button scanPath = new Button("Scan Path");
        Button setStart = new Button("Set Start");
        Button setEnd = new Button("Set End");
        Button clearObstacle = new Button("Clear Obstacles");
        bottom.getChildren().add(setStart);
        bottom.getChildren().add(setEnd);
        bottom.getChildren().add(drawPath);
        bottom.getChildren().add(scanPath);
        bottom.getChildren().add(clearObstacle);
        mainPane.setBottom(bottom);
        drawPath.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                matrixView.reFill();
                matrixView.drawPath(false);
            }
        });
        scanPath.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                matrixView.reFill();
                matrixView.drawPath(true);
            }
        });
        setStart.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                matrixView.setSettingStart(true);
            }
        });
        setEnd.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                matrixView.setSettingEnd(true);
            }
        });
        clearObstacle.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                matrixView.clearObstacle();
            }
        });
        Scene scene = new Scene(mainPane);
        primaryStage.setTitle("Matrix View");
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
