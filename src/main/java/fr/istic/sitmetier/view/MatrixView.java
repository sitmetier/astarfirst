package fr.istic.sitmetier.view;

import fr.istic.sitmetier.main.Matrix;
import fr.istic.sitmetier.main.PathMove;
import fr.istic.sitmetier.main.Square;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

import java.util.ArrayList;

/**
 * Created by erwann on 11/03/15.
 */
public class MatrixView {

    private Matrix matrix;
    private GridPane gridPane;
    private Group display;
    private int xStart;
    private int yStart;
    private int xEnd;
    private int yEnd;
    private boolean settingStart;
    private boolean settingEnd;
    private ArrayList<Square> paths;
    private PathMove pathMoveScan;
    private PathMove pathMoveTransit;
    private int[][] weightedMatrix;

    private static int SQUARE_SIZE = 10;
    private static int SQUARE_GAP = 1;

    public MatrixView(int columns, int rows){
        this.matrix = new Matrix(columns, rows);
        this.weightedMatrix = new int[columns][rows];
        this.gridPane = new GridPane();
        paths = new ArrayList<>();
        pathMoveScan = new PathMove(false);
        pathMoveTransit = new PathMove(true);
        gridPane.setMaxWidth(matrix.getSizeX());
        gridPane.setMaxHeight(matrix.getSizeY());
        gridPane.setHgap(SQUARE_GAP);
        display = new Group(gridPane);
        initMatrix();
        fillMatrix();
    }

    private void initMatrix(){
        for (int i = 0; i < matrix.getSizeX(); i++) {
            for (int j = 0; j < matrix.getSizeY(); j++) {
                if (matrix.hasObstacle(i, j)==1){
                    weightedMatrix[i][j] = -1;
                }else {
                    weightedMatrix[i][j] = 0;
                }
            }
        }
        weightedMatrix[xStart][yStart] = -2;
        weightedMatrix[xEnd][yEnd] = -3;
    }

    private void fillMatrix(){
        gridPane.getChildren().clear();
        for (int i = 0; i < matrix.getSizeX(); i++){
            for (int j = 0; j < matrix.getSizeY(); j++){
                if (weightedMatrix[i][j] > 0){
                    weightedMatrix[i][j] = 0;
                }
                gridPane.add(createSquare(i, j), i, j);
            }
        }
    }

    public void clearObstacle(){
        matrix.clearObstacle();
        for (int i = 0; i < matrix.getSizeX(); i++) {
            for (int j = 0; j < matrix.getSizeY(); j++) {
                if (!isStart(i,j) && !isEnd(i, j)) {
                    weightedMatrix[i][j] = 0;
                }
            }
        }
        reFill();
    }

    /*public void changeMatrix(int colomuns, int rows){
        matrix = new Matrix(colomuns, rows);
        setStart(0,0);
        setEnd(matrix.getSizeX() - 1, matrix.getSizeY() - 1);
        gridPane.getChildren().clear();
        gridPane.setMaxWidth(matrix.getSizeX());
        gridPane.setMaxHeight(matrix.getSizeY());
        fillMatrix();
    }*/

    public void reFill(){
        fillMatrix();
    }

    public void drawPath(boolean scan){
        if (scan){
            paths = pathMoveScan.scanPath(matrix, xStart, yStart, xEnd, yEnd);
        } else {
            paths = pathMoveTransit.mkPath(matrix, xStart, yStart, xEnd, yEnd);
        }
        if (paths != null && !paths.isEmpty()) {
            for (Square square : paths){
                int x = square.getPosition().getX();
                int y = square.getPosition().getY();
                if(!isStart(x, y) && !isEnd(x, y)) {
                    weightedMatrix[x][y] += 1;
                }
                gridPane.add(
                        createSquare(x,y),
                        x,y);

            }
        }
    }

    private SquareView createSquare(int x, int y){
        final SquareView squareView = new SquareView(x, y, SQUARE_SIZE);
        int pound = weightedMatrix[x][y];
        switch (pound){
            case -3:
                squareView.setFill(Color.RED);
                break;
            case -2:
                squareView.setFill(Color.BLUE);
                break;
            case -1:
                squareView.setFill(Color.BLACK);
                break;
            case 0:
                squareView.setFill(Color.WHITE);
                break;
            case 1:
                squareView.setFill(Color.GREENYELLOW);
                break;
            case 2:
                squareView.setFill(Color.LIME);
                break;
            case 3:
                squareView.setFill(Color.LIMEGREEN);
                break;
            case 4:
                squareView.setFill(Color.DARKGREEN);
                break;
            default:
                squareView.setFill(Color.DARKOLIVEGREEN);
                break;
        }
        squareView.setStroke(Color.SILVER);
        squareView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                int x = squareView.getSquareX();
                int y = squareView.getSquareY();
                if (settingStart){
                    setStart(x, y);
                    reFill();
                    settingStart = false;
                } else if (settingEnd){
                    setEnd(x, y);
                    reFill();
                    settingEnd = false;
                } else {
                    if (matrix.hasObstacle(x, y) == 1) {
                        matrix.setFree(x, y);
                        weightedMatrix[x][y] = 0;
                    } else if (matrix.hasObstacle(x, y) == 0) {
                        matrix.setObstacle(x, y);
                        weightedMatrix[x][y] = -1;
                    }
                    reFill();
                }
            }
        });
        return squareView;
    }

    public void setStart(int x, int y){
        weightedMatrix[xStart][yStart] = 0;
        xStart = x;
        yStart = y;
        weightedMatrix[xStart][yStart] = -2;
    }

    public void setEnd(int x, int y){
        weightedMatrix[xEnd][yEnd] = 0;
        xEnd = x;
        yEnd = y;
        weightedMatrix[xEnd][yEnd] = -3;
    }

    public boolean isEnd(int x, int y){
        return x == xEnd && y == yEnd;
    }

    public boolean isStart(int x, int y){
        return x == xStart && y == yStart;
    }

    public Group getDisplay() {
        return display;
    }

    public void setSettingStart(boolean settingStart) {
        this.settingStart = settingStart;
    }

    public void setSettingEnd(boolean settingEnd) {
        this.settingEnd = settingEnd;
    }
}
