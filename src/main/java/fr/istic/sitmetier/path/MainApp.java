package fr.istic.sitmetier.path;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.*;
import com.lynden.gmapsfx.shapes.Polygon;
import com.lynden.gmapsfx.shapes.PolygonOptions;
import com.lynden.gmapsfx.shapes.Polyline;
import com.lynden.gmapsfx.shapes.PolylineOptions;
import fr.istic.sitmetier.path.PathAdapter.PointGPS;
import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import netscape.javascript.JSObject;

import java.util.ArrayList;
import java.util.List;

public class MainApp extends Application implements MapComponentInitializedListener {

    protected GoogleMapView mapComponent;
    protected GoogleMap map;

    private Button btnZoomIn;
    private Button btnZoomOut;
    private Button btnPath;
    private Label lblZoom;
    private Label lblClick;
    private ComboBox<MapTypeIdEnum> mapTypeCombo;
  	private PointGPS pStart;
  	private PointGPS pEnd;

    @Override
    public void start(final Stage stage) throws Exception {
        mapComponent = new GoogleMapView();
        mapComponent.addMapInializedListener(this);
        
        BorderPane bp = new BorderPane();
        ToolBar tb = new ToolBar();

        btnZoomIn = new Button("Zoom In");
        btnZoomIn.setOnAction(e -> {
            map.zoomProperty().set(map.getZoom() + 1);
        });
        btnZoomIn.setDisable(true);

        btnZoomOut = new Button("Zoom Out");
        btnZoomOut.setOnAction(e -> {
            map.zoomProperty().set(map.getZoom() - 1);
        });
        btnZoomOut.setDisable(true);
        
        btnPath = new Button("Draw Path");
        btnPath.setOnAction(e -> {
            drawPath();
        });
        btnPath.setDisable(true);

        lblZoom = new Label();
        lblClick = new Label();
        
        mapTypeCombo = new ComboBox<>();
        mapTypeCombo.setOnAction( e -> {
           map.setMapType(mapTypeCombo.getSelectionModel().getSelectedItem() );
        });
        mapTypeCombo.setDisable(true);
        
        Button btnType = new Button("Map type");
        btnType.setOnAction(e -> {
            map.setMapType(MapTypeIdEnum.HYBRID);
        });
        tb.getItems().addAll(btnZoomIn, btnZoomOut, btnPath, mapTypeCombo,
                new Label("Zoom: "), lblZoom,
                new Label("Click: "), lblClick);

        bp.setTop(tb);
        bp.setCenter(mapComponent);

        Scene scene = new Scene(bp);
        stage.setScene(scene);
        stage.show();
    }
	
	private ArrayList<PointGPS> getGPSPath() {
		//Forbidden points initialization
		ArrayList<ArrayList<PointGPS>> forbiddenZonePoints = new ArrayList<>();

		ArrayList<PointGPS> polForbiddenZone = new ArrayList<PointGPS>();
		PointGPS pFougeres = new PointGPS(48.3512285,-1.6953465);
		PointGPS pMayenne = new PointGPS(48.3066204,-0.6140115);
		PointGPS pDomfront = new PointGPS(48.5788905,-0.6140655);
		polForbiddenZone.add(pFougeres);
		polForbiddenZone.add(pMayenne);
		polForbiddenZone.add(pDomfront);
		forbiddenZonePoints.add(polForbiddenZone);
	
		drawForbiddenZones(forbiddenZonePoints);

		//Origin North-West point
		PointGPS pNW = new PointGPS(48.8588589, -1.6884545);
		//South-East point
		PointGPS pSE = new PointGPS(48.1159156, 2.3470599);
		
		return PathAdapter.getPath(pNW, pSE, forbiddenZonePoints, pStart, pEnd, true);
	}
	
    @Override
    public void mapInitialized() {
        //Once the map has been loaded by the Webview, initialize the map details.
        LatLong center = new LatLong(48, 0.3);
        mapComponent.addMapReadyListener(() -> {
            // This call will fail unless the map is completely ready.
        });
        
        MapOptions options = new MapOptions();
        options.center(center)
                .mapMarker(true)
                .zoom(8)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(false)
                .mapType(MapTypeIdEnum.TERRAIN);

        map = mapComponent.createMap(options);
        
        lblZoom.setText(Integer.toString(map.getZoom()));
        map.zoomProperty().addListener((ObservableValue<? extends Number> obs, Number o, Number n) -> {
            lblZoom.setText(n.toString());
        });

        map.addUIEventHandler(UIEventType.click, (JSObject obj) -> {
            LatLong ll = new LatLong((JSObject) obj.getMember("latLng"));
            //System.out.println("LatLong: lat: " + ll.getLatitude() + " lng: " + ll.getLongitude());
            lblClick.setText(ll.toString());
        });

        btnZoomIn.setDisable(false);
        btnZoomOut.setDisable(false);
        btnPath.setDisable(false);
        mapTypeCombo.setDisable(false);
        
        mapTypeCombo.getItems().addAll( MapTypeIdEnum.ALL );
        
		//Rennes
		pStart = new PointGPS(48.1159156, -1.6884545);
		//Paris
		pEnd = new PointGPS(48.8588589, 2.3470599);
		
		drawMarkerAtPointGPS(pStart);
		drawMarkerAtPointGPS(pEnd);		
    }
    
    /**
     * Draws a marker over the given GPS point
     * @param point GPS point
     */
    private void drawMarkerAtPointGPS(PointGPS point){
    	 MarkerOptions markerOptions = new MarkerOptions();
         LatLong markerLatLong = new LatLong(point.lat, point.lng);
         markerOptions.position(markerLatLong)
                 .title("My new Marker")
                 .animation(Animation.DROP)
                 .visible(true);

         final Marker myMarker = new Marker(markerOptions);

         map.addMarker(myMarker);
    }
    
    private void drawPath(){
        LatLong[] ary =  listPointGPStoLatLongArray(getGPSPath()); 
        MVCArray mvc = new MVCArray(ary);

        PolylineOptions polyOpts = new PolylineOptions()
                .path(mvc)
                .strokeColor("green")
                .strokeWeight(2);

        Polyline poly = new Polyline(polyOpts);
        
        map.addUIEventHandler(poly, UIEventType.click, (JSObject obj) -> {
            LatLong ll = new LatLong((JSObject) obj.getMember("latLng"));
            //System.out.println("You clicked the line at LatLong: lat: " + ll.getLatitude() + " lng: " + ll.getLongitude());
        });
        
        map.addMapShape(poly);
    }
    
    /**
     * Converts a list of PointGPS objects into a LatLong objects array for Google Maps
     * @param pointGPSList
     * @return
     */
    private LatLong[] listPointGPStoLatLongArray(List<PointGPS> pointGPSList){
    	List <LatLong> listLatLong = new ArrayList<LatLong>();
		for (PointGPS point : pointGPSList) {
			LatLong pi = new LatLong(point.lat, point.lng);
			listLatLong.add(pi);
		}
        
        return listLatLong.toArray(new LatLong[listLatLong.size()]);
    }
    
    /**
     * Draw in map the forbidden zones using the given list of list of GPS points (polygons)
     * @param forbiddenPoints
     */
    private void drawForbiddenZones(ArrayList<ArrayList<PointGPS>> forbiddenPoints){
	    for (ArrayList<PointGPS> forbiddenPolygon: forbiddenPoints) {
	        LatLong[] pAry = listPointGPStoLatLongArray(forbiddenPolygon);
	        MVCArray pmvc = new MVCArray(pAry);
	
	        PolygonOptions polygOpts = new PolygonOptions()
	                .paths(pmvc)
	                .strokeColor("blue")
	                .strokeWeight(2)
	                .editable(false)
	                .fillColor("lightBlue")
	                .fillOpacity(0.5);
	
	        Polygon pg = new Polygon(polygOpts);
	
	        map.addMapShape(pg);
	        map.addUIEventHandler(pg, UIEventType.click, (JSObject obj) -> {
	            //polygOpts.editable(true);
	            pg.setEditable(!pg.getEditable());
	        });
		}
    }
    
    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //System.setProperty("java.net.useSystemProxies", "true");
        launch(args);
    }
}