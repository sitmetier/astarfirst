package fr.istic.sitmetier.path;

import fr.istic.sitmetier.main.DotPosition;
import fr.istic.sitmetier.main.Matrix;
import fr.istic.sitmetier.main.PathMove;
import fr.istic.sitmetier.main.Square;

import java.awt.Polygon;
import java.util.ArrayList;
import java.util.List;

public class PathAdapter {
	public static class PointGPS {
		double lat;
		double lng;
		
		public PointGPS(double lat, double lng) {
			super();
			this.lat = lat;
			this.lng = lng;
		}
		
		public String toString(){
			return lat + "," + lng;
			
		}
	}
	
	/**
	 * Calculates the shortest path between pStart and pEnd
	 * @param pNW origin point at zone north west
	 * @param pSE end point at zone south east
	 * @param forbiddenPoints list of list of points (polygons) of forbidden zone
	 * @param pStart start point
	 * @param pEnd end point
	 * @param isScan if true use scan algorithm
	 * @return
	 */
	public static ArrayList<PointGPS> getPath(
			PointGPS pNW, PointGPS pSE, 
			ArrayList<ArrayList<PointGPS>> forbiddenPoints, 
			PointGPS pStart, PointGPS pEnd, boolean isScan) {
		final int INIT_SQUARE_SIZE = 1; // size in meters
		final int MAX_MATRIX_SIZE = 268435455; //1024² * 256 - 1 to speed up calculation
		double sqSize = INIT_SQUARE_SIZE;
		long alpha = 1 ; // Coefficient to limit matrix size

		ArrayList<PointGPS> result = new ArrayList<>();
		
		double dx = gps2m(pNW.lat, pNW.lng, pNW.lat, pSE.lng);
		double dy = gps2m(pNW.lat, pNW.lng, pSE.lat, pNW.lng);

		long nbSqX = (long)Math.ceil(dx / INIT_SQUARE_SIZE);
		long nbSqY = (long)Math.ceil(dy / INIT_SQUARE_SIZE);

		//Alpha calculation
		long nbSq = nbSqX * nbSqY;
		if(nbSq > MAX_MATRIX_SIZE){
			alpha = nbSq / MAX_MATRIX_SIZE;
			nbSqX /= alpha;
			nbSqY /= alpha;
			sqSize = INIT_SQUARE_SIZE * alpha;
			nbSq = nbSqX * nbSqY;
		}
		
		int nbLines = (int)nbSqY, nbColumns = (int)nbSqX;
		List<DotPosition> forbiddenZone = new ArrayList<DotPosition>();
		for (List<PointGPS> zone : forbiddenPoints){
			List<DotPosition> dotPositions = new ArrayList<DotPosition>();
			for (PointGPS pointGPS :zone) {
				dotPositions.add(getDPos(pNW, pointGPS, sqSize));
			}
			forbiddenZone.addAll(getForbiddenDPos(nbColumns, nbLines, buildPolygon(dotPositions)));
		}
		
		// Test AStar
		Matrix realMatrix = new Matrix(nbColumns, nbLines);
		ArrayList<Square> pathFind;
		Square sqStart, sqEnd;
		
		DotPosition dpStart = getDPos(pNW, pStart, sqSize);
		DotPosition dpEnd = getDPos(pNW, pEnd, sqSize);
		
		sqStart = realMatrix.getSquareByPosition(dpStart.getX(), dpStart.getY()-1);
		sqEnd = realMatrix.getSquareByPosition(dpEnd.getX()-1, dpEnd.getY());
		
		for (DotPosition dp:forbiddenZone) {
			realMatrix.setObstacle(dp);
		}

		if (sqStart != null && sqEnd != null) {
			PathMove pathMove = new PathMove(!isScan);
			
			if (isScan) {
				pathFind = pathMove.scanPath(realMatrix,sqStart.getPosition().getX(),sqStart.getPosition().getY(), sqEnd.getPosition().getX(), sqEnd.getPosition().getY());
			} else {
				pathFind = pathMove.mkPath(realMatrix,sqStart.getPosition().getX(),sqStart.getPosition().getY(), sqEnd.getPosition().getX(), sqEnd.getPosition().getY());
			}
			
			if (pathFind == null) {
				//System.out.println("Pas de chemin possible");
				return null;
			}
			pathFind.add(sqEnd);

			for (Square s : pathFind) {
				//System.out.println(s.getPosition());
				PointGPS pointGPS = pos2gps(pNW, pSE, nbColumns, nbLines, s.getPosition());
				//System.out.println(pointGPS);
				result.add(pointGPS);
			}
		}else {
			//System.out.println("Erreur sur les positions de départ ou arrivée");
		}
		return result;
	}
	
	/**
	 * Maps a GPS to Matrix coordinates.
	 * Negative or bigger than matrix size values may be returned 
	 * if the point is outside the defined area.
	 * @param pNW Origin point from coordinate system (0, 0) position in matrix
	 * @param point Current GPS point to map
	 * @param sqSize Size of each square 
	 * @return Point mapped in matrix position coordinates
	 */
	public static DotPosition getDPos(PointGPS pNW, PointGPS point, double sqSize) {
		double deltaLat = pNW.lat - point.lat;
		double deltaLng = point.lng - pNW.lng;

		double dx = gps2m(pNW.lat, pNW.lng, pNW.lat, point.lng);
		double dy = gps2m(pNW.lat, pNW.lng, point.lat, pNW.lng);
		
		int xPos = (int)Math.floor(dx / sqSize) * (int) Math.signum(deltaLng);
		int yPos = (int)Math.floor(dy / sqSize) * (int) Math.signum(deltaLat);
		return new DotPosition(xPos, yPos);
	}
	
	/**
	 * Build a polygon object from a list of given vertices in matrix positions
	 * @param vertices
	 * @return a java.awt.Polygon object
	 */
	public static Polygon buildPolygon(List<DotPosition> vertices){
		Polygon polygon = new Polygon();
		for (DotPosition vertex : vertices){
			polygon.addPoint(vertex.getX(), vertex.getY());
		}
		return polygon;
	}
	
	/**
	 * Get positions inside matrix equivalent of points contained inside the forbidden zone
	 * @param nbSqX matrix size in X
	 * @param nbSqY matrix size in Y
	 * @param forbiddenZone Polygon that defines the forbidden zone
	 * @return list of positions inside matrix
	 */
	public static List<DotPosition> getForbiddenDPos(int nbSqX, int nbSqY, Polygon forbiddenZone){
		List<DotPosition> forbiddenDPos = new ArrayList<DotPosition>();
		
		for (int i=0; i< nbSqX; i++){
			for (int j=0; j< nbSqY; j++){
				if (forbiddenZone.contains(i, j)){
					forbiddenDPos.add(new DotPosition(i, j));
				}
			}
		}
		return forbiddenDPos;
	}

	/**
	 * Get the distance between two GPS points in meters 
	 * @param lat_a GPS latitude of first point
	 * @param lng_a GPS longitude of first point
	 * @param lat_b GPS latitude of second point
	 * @param lng_b GPS longitude of second point
	 * @return distance between two GPS points in meters 
	 */
	private static double gps2m(double lat_a, double lng_a, double lat_b, double lng_b) {
		double angleLatA = Math.toRadians(lat_a);
	    double angleLngA = Math.toRadians(lng_a);
	    double angleLatB = Math.toRadians(lat_b);
	    double angleLngB = Math.toRadians(lng_b);

	    double t1 = Math.cos(angleLatA)*Math.cos(angleLngA)*Math.cos(angleLatB)*Math.cos(angleLngB);
	    double t2 = Math.cos(angleLatA)*Math.sin(angleLngA)*Math.cos(angleLatB)*Math.sin(angleLngB);
	    double t3 = Math.sin(angleLatA)*Math.sin(angleLatB);
	    double tt = Math.acos(t1 + t2 + t3);
	   
	    return 6366000*tt;
	}
	
	/**
	 * Convert the position of a square inside the matrix into GPS coordinates
	 * @param pNW North West point of mapped zone
	 * @param pSE South East point of mapped zone
	 * @param nbSqX Number of squares in X-axis (longitude)
	 * @param nbSqY Number of squares in Y-axis (latitude)
	 * @param point Position to convert
	 * @return
	 */
	public static PointGPS pos2gps(PointGPS pNW, PointGPS pSE, int nbSqX, int nbSqY, DotPosition point) {
		double angleLng = (pSE.lng - pNW.lng)/nbSqX;
		double angleLat = (pSE.lat - pNW.lat)/nbSqY;
		double lng = pNW.lng + point.getX() * angleLng;
		double lat = pNW.lat + point.getY() * angleLat;
		PointGPS result = new PointGPS(lat, lng);
		return result;
	}
}
