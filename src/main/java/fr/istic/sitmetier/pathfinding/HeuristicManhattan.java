package fr.istic.sitmetier.pathfinding;

import fr.istic.sitmetier.main.DotPosition;
import fr.istic.sitmetier.main.Square;

/**
 * Created by david on 12/02/15.
 */
public class HeuristicManhattan implements Heuristic<Square> {
    @Override
    public int getHeuristic(Square currentPos, Square goalPos) {
        double dx, dy;
        int pas = 1;
        DotPosition curDP, goalDP;

        curDP = currentPos.getPosition();
        goalDP = goalPos.getPosition();

        dx = Math.abs(goalDP.getX() - curDP.getX());
        dy = Math.abs(goalDP.getY() - curDP.getY());

        return (int) (pas * (dx + dy));
    }
}
