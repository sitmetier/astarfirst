package fr.istic.sitmetier.pathfinding;

import fr.istic.sitmetier.main.Square;

import java.util.ArrayList;

/**
 * Created by david on 10/03/15.
 */
public class NextMoveDiagonal implements NextMove<Square> {
    @Override
    public ArrayList<Square> genMoves(Square square) {
        ArrayList<Square> squares;
        Square nextSquare;


        squares = new ArrayList<Square>();

        nextSquare = square.getMyMatrix().getLeftUpSquare(square);
        addToSqList(nextSquare, squares);

        nextSquare = square.getMyMatrix().getLeftDownSquare(square);
        addToSqList(nextSquare, squares);

        nextSquare = square.getMyMatrix().getRightDownSquare(square);
        addToSqList(nextSquare, squares);

        nextSquare = square.getMyMatrix().getRightUpSquare(square);
        addToSqList(nextSquare, squares);

        nextSquare = square.getMyMatrix().getLeftSquare(square);
        addToSqList(nextSquare, squares);

        nextSquare = square.getMyMatrix().getRightSquare(square);
        addToSqList(nextSquare, squares);

        nextSquare = square.getMyMatrix().getUpSquare(square);
        addToSqList(nextSquare, squares);

        nextSquare = square.getMyMatrix().getDownSquare(square);
        addToSqList(nextSquare, squares);

        return squares;
    }

    private boolean addToSqList(Square square, ArrayList<Square> squares) {
        if (square == null) {
            return (false);
        }

        if (square.isEmpty() == true) {
            squares.add(square);
        }
        return (true);
    }
}
