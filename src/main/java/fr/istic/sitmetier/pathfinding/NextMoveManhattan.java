package fr.istic.sitmetier.pathfinding;

import fr.istic.sitmetier.main.Square;

import java.util.ArrayList;

/**
 * Created by david on 12/02/15.
 */
public class NextMoveManhattan implements NextMove<Square> {
    @Override
    public ArrayList<Square> genMoves(Square square) {
        ArrayList<Square> squares;
        Square nextSquare;


        squares = new ArrayList<Square>();

        nextSquare = square.getMyMatrix().getLeftSquare(square);
        if ((nextSquare != null) && (nextSquare.isEmpty() == true)) {
            squares.add(nextSquare);
        }

        nextSquare = square.getMyMatrix().getRightSquare(square);
        if ((nextSquare != null) && (nextSquare.isEmpty() == true)) {
            squares.add(nextSquare);
        }

        nextSquare = square.getMyMatrix().getUpSquare(square);
        if ((nextSquare != null) && (nextSquare.isEmpty() == true)) {
            squares.add(nextSquare);
        }

        nextSquare = square.getMyMatrix().getDownSquare(square);
        if ((nextSquare != null) && (nextSquare.isEmpty() == true)) {
            squares.add(nextSquare);
        }

        return squares;
    }
}
