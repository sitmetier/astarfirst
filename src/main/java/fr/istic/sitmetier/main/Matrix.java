package fr.istic.sitmetier.main;

/**
 * Created by david on 11/02/15.
 */
public class Matrix {
    private Square pathTab[][];
    private Integer nbElem = 0;
    private Integer sizeX;
    private Integer sizeY;


    public Matrix(Integer column, Integer line) {
        int idx, i, j;
        DotPosition dp;
        Square curSq;

        this.pathTab = new Square[column][line];

        for (idx = 0, i = 0; i < column; i++) {

            for (j = 0; j < line; j++, idx++) {
                dp = new DotPosition(i, j);
                curSq = new Square(idx, dp, true, this);

                this.pathTab[i][j] = curSq;
            }
        }

        this.nbElem = idx;
        this.sizeX = column;
        this.sizeY = line;
    }

    public Square getSquareByPosition(int x, int y) {
        if ((x >= this.sizeX) || (y >= this.sizeY) || (x < 0) || (y < 0)) {
            return null;
        }

        return (this.pathTab[x][y]);
    }

    public Square getSquareByPosition(DotPosition dot) {
        return (getSquareByPosition(dot.getX(), dot.getY()));
    }

    public Square getLeftSquare(Square square) {
        int x, y;


        if (square == null) return (null);

        x = square.getPosition().getX();
        y = square.getPosition().getY();

        if (x <= 0) {
            return null;
        }

        x--;

        return (getSquareByPosition(x, y));
    }

    public Square getRightSquare(Square square) {
        int x, y;


        if (square == null) return (null);

        x = square.getPosition().getX();
        y = square.getPosition().getY();

        x++;

        return (getSquareByPosition(x, y));
    }

    public Square getUpSquare(Square square) {
        int x, y;


        if (square == null) return (null);

        x = square.getPosition().getX();
        y = square.getPosition().getY();

        if (y <= 0) {
            return null;
        }

        y--;

        return (getSquareByPosition(x, y));
    }

    public Square getDownSquare(Square square) {
        int x, y;


        if (square == null) return (null);

        x = square.getPosition().getX();
        y = square.getPosition().getY();

        y++;

        return (getSquareByPosition(x, y));
    }

    public Square getLeftUpSquare(Square square) {
        return (getUpSquare(getLeftSquare(square)));
    }

    public Square getLeftDownSquare(Square square) {
        return (getDownSquare(getLeftSquare(square)));
    }

    public Square getRightUpSquare(Square square) {
        return (getUpSquare(getRightSquare(square)));
    }

    public Square getRightDownSquare(Square square) {
        return (getDownSquare(getRightSquare(square)));
    }

    public void setObstacle(int x, int y) {
        Square square = getSquareByPosition(x, y);
        if (square != null) {
            square.setEmpty(false);
        }
    }

    public void setObstacle(DotPosition dot) {
        setObstacle(dot.getX(), dot.getY());
    }

    public void setFree(int x, int y) {
        Square square = getSquareByPosition(x, y);
        if (square != null) {
            square.setEmpty(true);
        }
    }

    public void setFree(DotPosition dot) {
        setFree(dot.getX(), dot.getY());
    }

    public int hasObstacle(int x, int y) {
        Square square = getSquareByPosition(x, y);
        if (square != null) {
            return !square.isEmpty() ? 1 : 0;
        }
        return -1;
    }


    public int hasObstacle(DotPosition dot) {
        return (hasObstacle(dot.getX(), dot.getY()));
    }



    public void dispPathTab() {
        int idx, i, j;
        Square curSq;

        if (this.nbElem <= 0) {
            return;
        }

        for (idx = 0, i = 0; i < this.sizeX; i++) {
            for (j = 0; j < this.sizeY; j++, idx++) {
                curSq = this.pathTab[i][j];
            }
        }

    }

    public Integer getNbElem() {
        return nbElem;
    }

    public Integer getSizeX() {
        return sizeX;
    }

    public Integer getSizeY() {
        return sizeY;
    }

    public String toString () {
        String line = "";
        for (int i = 0; i < this.sizeY; i++) {
            for (int j = 0; j < this.sizeX; j++) {
                line += this.hasObstacle(j, i) == 1 ? "1" : "0";
            }
            line += "\n";
        }
        return line;
    }

    public void clearObstacle(){
        for (int i = 0; i < sizeX; i++){
            for (int j = 0; j < sizeY; j++){
                setFree(i, j);
            }
        }
    }

}
