package fr.istic.sitmetier.main;

import fr.istic.sitmetier.pathfinding.*;

import java.util.ArrayList;

/**
 * Created by david on 13/03/15.
 */
public class PathMove {

    private Astar<Square, NextMove<Square>, Heuristic<Square>> astar;
    private NextMove<Square> nextMove;
    private Heuristic<Square> heuristic;


    public PathMove() {
//        nextMove = new NextMoveManhattan();
//        heuristic = new HeuristicManhattan();
        nextMove = new NextMoveDiagonal();
        heuristic = new HeuristicDiagonal();
        astar = new Astar<Square, NextMove<Square>, Heuristic<Square>>();
    }

    public PathMove(boolean isTransit) {

        if (isTransit) {
            heuristic = new HeuristicDiagonalStd();
            nextMove = new NextMoveDiagonal();
        }
        else {
//            heuristic = new HeuristicDiagonal();
//            nextMove = new NextMoveDiagonal();
            nextMove = new NextMoveManhattan();
            heuristic = new HeuristicManhattan();
        }
        astar = new Astar<Square, NextMove<Square>, Heuristic<Square>>();
    }


    public ArrayList<Square> mkPath(Matrix matrix, int sx, int sy, int ex, int ey, boolean withEnd) {
        Square              sqStart, sqEnd;
        ArrayList<Square>   pathFind;


        // Verifications
        sqStart = matrix.getSquareByPosition(sx, sy);
        sqEnd = matrix.getSquareByPosition(ex, ey);
        if (sqStart == null || sqEnd == null) {
            return (null);
        }

        // Determination du chemin
        pathFind = astar.shortestPath(sqStart, sqEnd, nextMove, heuristic);
        if (pathFind == null) {
            System.out.println("Pas de chemin possible");
            return (null);
        }

        // Ajout du dernier point si demande
        if (withEnd) {
            pathFind.add(sqEnd);
        }

        return(pathFind);
    }


    public ArrayList<Square> mkPath(Matrix matrix, DotPosition sDot, DotPosition eDot, boolean withEnd) {
        return(mkPath(matrix, sDot.getX(), sDot.getY(), eDot.getX(), eDot.getY(), withEnd));
    }

    public ArrayList<Square> mkPath(Matrix matrix, int sx, int sy, int ex, int ey) {
        return(mkPath(matrix, sx, sy, ex, ey, true));
    }

    public ArrayList<Square> mkPath(Matrix matrix, DotPosition sDot, DotPosition eDot) {
        return(mkPath(matrix, sDot.getX(), sDot.getY(), eDot.getX(), eDot.getY(), true));
    }

    public ArrayList<Square> scanPath(Matrix matrix, int sx, int sy, int ex, int ey) {
        final int           minInter = 2, maxHMove = 10;
        ArrayList<Square>   pathScan, pathMove;
        DotPosition         sDot, eDot;
        boolean             bascule;
        Square              square;
        int                 nbHMove, inter, smx, emx, smy, emy, yf;


        // Calcule du nombre de passages pour trouver l'intervale entre 2 passages du balayage
        if (sy > ey) {
            nbHMove = (sy - ey) / minInter;
            if (nbHMove > maxHMove) {
                inter = (sy - ey) / maxHMove;
            }
            else {
                inter = minInter;
            }

            // Determiner le sens d'application du balayage
            inter = - inter;
        }
        else {
            nbHMove = (ey - sy) / minInter;
            if (nbHMove > maxHMove) {
                inter = (ey - sy) / maxHMove;
            }
            else {
                inter = minInter;
            }

            // Determiner le sens d'application du balayage
            // inter = - inter;

        }
        yf = ey;
// System.out.println("sy = " + sy + ", ey = " + ey + ", Inter = " + inter);

        pathScan = new ArrayList<Square>();
        bascule = false;
        smx = sx;
        emx = ex;
        emy = sy + inter;
//        for (smy = sy; emy < yf; emy += inter) {
        for (smy = sy; ; emy += inter) {
            if ((sy > ey) && (emy < yf)) {
                break;
            }
            if ((sy <= ey) && (emy >= yf)) {
                break;
            }

            // Initialisation
            sDot = new DotPosition(smx, smy);
            eDot = new DotPosition(emx, emy);

            pathMove = scanPart(matrix, sDot, eDot);
            if (pathMove == null) {
                return null;
            }
            pathScan.addAll(pathMove);
            square = pathMove.get(pathMove.size()-1);
            if (square == null) {
                return null;
            }

            if (bascule == false) {
                bascule = true;
                emx = sx;
            }
            else {
                bascule = false;
                emx = ex;
            }
            smx = eDot.getX();
            smy = eDot.getY();
//System.out.println("smx = " + smx + ", smy = " + smy);
//System.out.println("emx = " + emx + ", emy = " + emy);

        }

        // Mouvement final
        pathMove = mkPath(matrix, smx, smy, ex, ey, true);
        if (pathMove == null) {
            return null;
        }
        pathScan.addAll(pathMove);

        return (pathScan);
    }

    public ArrayList<Square> scanPath(Matrix matrix, DotPosition sDot, DotPosition eDot) {
        return (scanPath(matrix, sDot.getX(), sDot.getY(), eDot.getX(), eDot.getY()));
    }

    // Fabrication d'un trajet horizontal suivit d'un trajet vertical
    // Attention, cette fonction modifie les entrees sDot et eDot
    public ArrayList<Square> scanPart(Matrix matrix, DotPosition sDot, DotPosition eDot) {
        ArrayList<Square>   pathScan, pathMove;
        boolean             deadzone;
        int                 smx, emx, smy, emy, sx, ex, it;


        // Initialisations
        pathScan = new ArrayList<Square>();
        pathMove = null;
        deadzone = false;
        smx = sDot.getX();
        emx = eDot.getX();
        smy = sDot.getY();
        emy = smy;
        sx = sDot.getX();
        ex = eDot.getX();

        // Positionnement du sens d'iterration pour la recherche du dernier point d'un trajet
        if (emx > smx) {
            it = -1;
        }
        else {
            it = 1;
        }


        // Rechercher le chemin horizontal en tenant compte des eventuels espace occupée
        // au niveau du point d'arrivée
        while (emx != sx) {
            if (matrix.hasObstacle(emx, emy) == 0) {
                if (deadzone == false) {
                    pathMove = mkPath(matrix, smx, smy, emx, emy, false);
                    if (pathMove != null) {
                        break;
                    }
                    deadzone = true;
                }
            }
            else {
//System.out.println("Obstacle 1");
                deadzone = false;
            }
            emx += it;
        }

        // Ajout du chemin s'il existe
        if (pathMove != null) {
            pathScan.addAll(pathMove);
        }

        // Initialisations
        deadzone = false;
        smx = emx;
        emx = ex;
        emy = eDot.getY();

        // Rechercher le chemin vertical en tenant compte des eventuels espace occupée
        // au niveau du point d'arrivée
        while (emx != sx) {
            if (matrix.hasObstacle(emx, emy) == 0) {
                if (deadzone == false) {
                    pathMove = mkPath(matrix, smx, smy, emx, emy, false);
                    if (pathMove != null) {
                        break;
                    }
                }
                deadzone = true;
            }
            else {
//System.out.println("Obstacle 2");
                deadzone = false;
            }
            emx += it;
        }

        // Ajout du chemin s'il existe
        if (pathMove != null) {
            pathScan.addAll(pathMove);
        }

        // Donner la position du point suivant
        eDot.setX(emx);
        // eDot.setY(emy);

        return (pathScan);
    }


    // Fabrication d'un trajet horizontal en modifiant le point d'arrivee si celui-ci est occupe
    // Attention, cette fonction modifie les entrees sDot et eDot
    private ArrayList<Square> mkApproxPath(Matrix matrix, DotPosition sDot, DotPosition eDot, boolean withEnd) {
        ArrayList<Square>   pathMove = null;
        boolean             deadzone;
        int                 it;


        // Initialisations
        deadzone = false;

        // Positionnement du sens d'iterration pour la recherche du dernier point d'un trajet
        if (eDot.getX() > sDot.getX()) {
            it = -1;
        }
        else {
            it = 1;
        }

        // Recherche du dernier point valide et determination du chemin
        while (eDot.getX() != sDot.getX()) {
            if (matrix.hasObstacle(eDot) == 0) {
                if (deadzone == false) {
                    pathMove = mkPath(matrix, sDot, eDot, false);
                    if (pathMove != null) {
                        break;
                    }
                    deadzone = true;
                }
            }
            else {
                deadzone = false;
            }
            eDot.setX(eDot.getX() + it);
        }

        return (pathMove);
    }


    // Fabrication d'un trajet horizontal suivit d'un trajet vertical via dichotomie
    // Attention, cette fonction modifie les entrees sDot et eDot
    // DEPRECATED : NON UTILISEE
    private ArrayList<Square> scanPartDico(Matrix matrix, DotPosition sDot, DotPosition eDot) {
        ArrayList<Square>   pathScan, pathMove, pathNext;
        int                 smx, emx, smy, emy, s, sx, ex;


        // Initialisations
        pathScan = new ArrayList<Square>();
        pathNext = null;
        smx = sDot.getX();
        emx = eDot.getX();
        smy = sDot.getY();
        emy = eDot.getY();
        sx = sDot.getX();
        ex = eDot.getX();
        s = ex;


        // Rechercher le chemin horizontal en tenant compte des eventuels espace occupée
        // au niveau du point d'arrivée
        pathMove = mkPath(matrix, smx, smy, emx, smy, false);
        while (pathMove == null) {
            if (pathNext == null) {
                emx = (s + sx) / 2;
            }
            else {
                emx = (s + ex) / 2;
            }

            if ((emx == sx) || (emx == ex)) {
                emx = s;
                pathMove = pathNext;
                break;
            }
            //System.out.println("emx = " + emx + ", s = " + s);
            if (emx == s) {
                pathMove = pathNext;
                break;
            }
            s = emx;
            pathNext = mkPath(matrix, smx, smy, emx, smy, false);
        }
        // Ajout du chemin s'il existe
        if (pathMove != null) {
            pathScan.addAll(pathMove);
        }

        // Initialisations
        pathNext = null;
        smx = emx;
        emx = ex;
        s = ex;

        // Rechercher le chemin vertical en tenant compte des eventuels espace occupée
        // au niveau du point d'arrivée
        pathMove = mkPath(matrix, smx, smy, emx, emy, false);
        while (pathMove == null) {
            if (pathNext == null) {
                emx = (s + sx) / 2;
            }
            else {
                emx = (s + ex) / 2;
            }

            if ((emx == sx) || (emx == ex)) {
                emx = s;
                pathMove = pathNext;
                break;
            }

            if (emx == s) {
                pathMove = pathNext;
                break;
            }
            s = emx;

            pathNext = mkPath(matrix, smx, smy, emx, emy, false);
        }

        // Ajout du chemin s'il existe
        if (pathMove != null) {
            pathScan.addAll(pathMove);
        }

        // Donner la position du point suivant
        eDot.setX(emx);
        // eDot.setY(emy);

        return (pathScan);
    }


}
