package fr.istic.sitmetier.main;

/**
 * Created by david on 11/02/15.
 */
public class Square implements Comparable<Square> {
    private Integer number;
    private DotPosition position;
    private boolean empty;
    private Matrix myMatrix;


    public Square(Integer number, DotPosition position, boolean empty, Matrix myMatrix) {
        this.number = number;
        this.position = position;
        this.empty = empty;
        this.myMatrix = myMatrix;

    }

    public Integer getNumber() {
        return this.number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public DotPosition getPosition() {
        return this.position;
    }

    public void setPosition(DotPosition position) {
        this.position = position;
    }

    public boolean isEmpty() {
        return this.empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    public Matrix getMyMatrix() {
        return myMatrix;
    }

    public void setMyMatrix(Matrix myMatrix) {
        this.myMatrix = myMatrix;
    }

    @Override
    public int compareTo(Square o) {
        if (this.number == o.getNumber()) {
            return 0;
        }

        if (this.number < o.getNumber()) {
            return -1;
        }

        return 1;
    }
}
