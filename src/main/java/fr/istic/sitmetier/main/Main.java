package fr.istic.sitmetier.main;

import java.util.ArrayList;

/**
 * Created by david on 11/02/15.
 */
public class Main {
    private static Matrix pathMatrix;
    private static PathMove pathMove;

    public static void main(String[] args) {
        ArrayList<Square> pathFind, finalPathFind;
        Square sqStart, sqEnd;


        pathMatrix = new Matrix(100, 10);
        pathMove = new PathMove();

//        pathMatrix.dispPathTab();

        // Pour test
        pathMatrix.setObstacle(5, 0);
        pathMatrix.setObstacle(6, 0);
        pathMatrix.setObstacle(9, 1);
        pathMatrix.setObstacle(15, 2);
        pathMatrix.setObstacle(45, 3);
        pathMatrix.setObstacle(35, 4);
        pathMatrix.setObstacle(25, 5);
//        pathMatrix.setObstacle(1, 4);
//        pathMatrix.setObstacle(1, 5);
//        pathMatrix.setObstacle(1, 6);
        pathMatrix.setObstacle(0, 7);
        pathMatrix.setObstacle(74, 5);
        pathMatrix.setObstacle(75, 5);
        pathMatrix.setObstacle(76, 5);
        pathMatrix.setObstacle(77, 5);
        pathMatrix.setObstacle(78, 5);


        System.out.println(pathMatrix.toString());

        pathFind = pathMove.mkPath(pathMatrix, 0, 0, 78, 0);
        if (pathFind == null) {
            return;
        }
        finalPathFind = pathFind;

        pathFind = pathMove.mkPath(pathMatrix, 78, 0, 78, 3);
        if (pathFind == null) {
            return;
        }
        finalPathFind.addAll(pathFind);

        pathFind = pathMove.mkPath(pathMatrix, 78, 3, 0, 3);
        if (pathFind == null) {
            return;
        }
        finalPathFind.addAll(pathFind);

        pathFind = pathMove.mkPath(pathMatrix, 0, 3, 0, 4);
        if (pathFind == null) {
            return;
        }
        finalPathFind.addAll(pathFind);

        pathFind = pathMove.mkPath(pathMatrix, 0, 4, 10, 9);
        if (pathFind == null) {
            return;
        }
        finalPathFind.addAll(pathFind);


        dispPathInAscii(pathMatrix, finalPathFind);


        pathMatrix.setObstacle(78, 0);
//        pathMatrix.setObstacle(78, 1);
//        pathMatrix.setObstacle(78, 2);
//        pathMatrix.setObstacle(0, 2);
        pathMatrix.setObstacle(0, 4);

        pathMove = new PathMove(false);
        finalPathFind = pathMove.scanPath(pathMatrix, 0, 0, 78, 9);
        dispPathInAscii(pathMatrix, finalPathFind);

    }

    private static void dispPathInAscii(Matrix matrix, ArrayList<Square> pathFind) {
        DotPosition pCur, pPrec;
        String dispTab[][];
        int i, j;


        // Verifier s'il y a un chemin
        if (pathFind == null) {
            return;
        }

        // Affichage des points du trajet
        for (Square s : pathFind) {
            System.out.println("" + s.getPosition() + " " + s.getNumber());
        }


        // Initialisations de la table d'affichage du trajet
        dispTab = new String[matrix.getSizeX() + 1][matrix.getSizeY() + 1];
        for (i = 0; i <= matrix.getSizeX(); i++) {
            for (j = 0; j <= matrix.getSizeY(); j++) {
                dispTab[i][j] = " ";
            }
        }

        // Ajout des obstacles
        for (i = 0; i <= matrix.getSizeX() - 2; i++) {
            for (j = 0; j <= matrix.getSizeY() - 2; j++) {
                if (matrix.hasObstacle(i, j) >= 1) {
                    dispTab[i + 2][j + 1] = ".";
                }
            }
        }

        // Creation de l'Affichage du trajet en ASCII dans la table
        pPrec = null;
        for (Square s : pathFind) {
            pCur = s.getPosition();
            if (pPrec == null) {
                pPrec = pCur;
                continue;
            }

            if (pCur.getX() > pPrec.getX()) {
                if (pCur.getY() > pPrec.getY()) {
                    dispTab[pCur.getX() + 1][pCur.getY() + 1] = "\\";
                } else if (pCur.getY() < pPrec.getY()) {
                    dispTab[pCur.getX() + 1][pCur.getY() + 2] = "/";
                } else {
                    dispTab[pCur.getX() + 1][pCur.getY() + 1] = "_";
                }
            } else if (pCur.getX() < pPrec.getX()) {
                if (pCur.getY() > pPrec.getY()) {
                    dispTab[pCur.getX() + 1][pCur.getY() + 1] = "/";
                } else if (pCur.getY() < pPrec.getY()) {
                    dispTab[pCur.getX() + 1][pCur.getY() + 2] = "\\";
                } else {
                    dispTab[pCur.getX() + 1][pCur.getY() + 1] = "_";
                }
            } else {
                if (pCur.getY() > pPrec.getY()) {
                    dispTab[pCur.getX() + 1][pCur.getY() + 1] = "|";
                } else if (pCur.getY() < pPrec.getY()) {
                    dispTab[pCur.getX() + 1][pCur.getY() + 1] = "|";
                } else {
                }
            }
            pPrec = pCur;
        }

        // Affichage du trajet via la table
        for (j = 0; j <= matrix.getSizeY(); j++) {
            for (i = 0; i <= matrix.getSizeX(); i++) {
                System.out.print(dispTab[i][j]);
            }
            System.out.println("");
        }
    }

}
