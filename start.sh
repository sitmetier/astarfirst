#!/bin/bash

if [ "$#" = "1" ]; then
	if [ "$1" = "map" ]; then
		java -jar target/map-jar-with-dependencies.jar
	elif [ "$1" = "matrix" ]; then
		java -jar target/matrix-jar-with-dependencies.jar
	else
		echo "L'argument spécifié est incorrect"
		echo "Arguments possibles : map ou matrix"
	fi
else
	echo "Le nombre d'arguments spécifiés est incorrect"
	echo "Arguments possibles : map ou matrix"
fi
